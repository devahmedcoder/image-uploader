import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: process.env.APIKEY,
    authDomain: "image-uploader-devahmedcoder.firebaseapp.com",
    projectId: "image-uploader-devahmedcoder",
    storageBucket: "image-uploader-devahmedcoder.appspot.com",
    messagingSenderId: "773213215918",
    appId: process.env.APPID,
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const ref = firebase.storage().ref();


export default ref;
