import React, { useState } from 'react'
import image from '../img/image.svg';
import ref from './Firebase';
import ok from '../img/ok.svg';

const Upload = () => {

    const [urlImage, setUrlImage] = useState(null);
    const [loader, setLoader] = useState(null);
    const [dropFile, setDropFile] = useState(null);

    const uploadImage = () => {

        let file = document.querySelector("#inputImage").files[0];
        if (file || dropFile) {
            if (dropFile) {
                file = dropFile;
            }
            setLoader(1);
            const name = new Date() + ' - ' + file.name;
            const metadata = { contentType: file.type }
            const task = ref.child(name).put(file, metadata);

            task.then(snapshot => snapshot.ref.getDownloadURL())
                .then(url => {
                    setLoader(null);
                    setUrlImage(url);
                    const imageElement = document.querySelector('#image');
                    imageElement.src = url;
                }
                )
        }

    }

    const allowDrop = (ev) => {
        ev.preventDefault();
    }

    const dropHandler = (ev) => {

        ev.preventDefault();

        if (/^image/.test(ev.dataTransfer.items[0].type)) {
            var file = ev.dataTransfer.items[0].getAsFile()
            setDropFile(file);
            const imageElement = document.querySelector('#image');
            let reader = new FileReader();
            reader.readAsDataURL(file)
            reader.onload = (e) => {
                imageElement.src = e.target.result
            }
        };
    }

    const loadImage = (e) => {

        if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                const imageElement = document.querySelector('#image');
                imageElement.src = e.target.result
            };
            reader.readAsDataURL(e.target.files[0]);
        }
    }

    if (urlImage) {
        return (
            <div className="container">
                <img id="imgOk" src={ok} />
                <p className="title">Uploaded Successfully!</p>
                <div className="box">
                    <img id="image" src="" />
                </div>
                <div>
                    <input type="text" value={urlImage} id="myInput" readOnly />
                    <button onClick={() => navigator.clipboard.writeText(urlImage)}>Copy link</button>
                </div>
            </div>
        )
    } else if (loader && (!urlImage)) {
        return <div id="loader"></div>
    } else {
        return (
            <div className="container">
                <p className="title">Upload your image</p>
                <p className="samp">File should be Jpeg, png...</p>

                <div className="box" id="drop_zone"
                    onDrop={(e) => dropHandler(e)}
                    onDragOver={(e) => allowDrop(e)}>
                    <img id="image" src={image} />
                    <p className="imgTitle">Drag & Drop your image here</p>
                </div>

                <p className="imgTitle">Or</p>
                <input id="inputImage" type="file" accept="image/*" onChange={(e) => loadImage(e)} />
                <button onClick={() => uploadImage()}>Upload</button>
            </div>
        )
    }
}

export default Upload;
